package com.epam.tat.calculator.impl;

import com.epam.tat.calculator.ICalculator;

public class Calculator implements ICalculator {

    private final int precision;

    public Calculator(int precision) {
        this.precision = precision;
    }

    @Override
    public double add(double a, double b) {
        return round(a + b);
    }

    @Override
    public double subtract(double a, double b) {
        return round(a - b);
    }

    @Override
    public double multiply(double a, double b) {
        return round(a * b);
    }

    @Override
    public double divide(double a, double b) {
        return round(a / b);
    }

    private double round(double number) {
        return Double.parseDouble(String.format("%.2f", number));
    }

}
